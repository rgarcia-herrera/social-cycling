# Infraestructura ciclista virtual: fuerza en números para viajar seguras

# Virtual cycling infrastructure: strength in numbers for safer rides

## Resumen

Es notoriamente sabido que 1. un estilo de vida saludable requiere
ejercicio cotidiano y 2. el medio ambiente de cualquier ciudad moderna
mejora con menos automóviles. Luego, hay que reemplazar automóviles por
bicicletas.

Un obstáculo sustancial para la adopción masiva de la bicicleta como
medio de transporte es la asimetría de seguridad que existe entre
ciclistas y motoristas al transitar la misma red de calles: un
accidente sin consecuencias para un motorista fácilmente puede
resultar fatal para una ciclista.

Este artículo presenta el diseño de una aplicación móvil que fomenta
la coordinación espontánea de convoyes de ciclistas. El razonamiento
es que, usando nuestra fuerza en números, viajaremos más seguras en
ciudades sin ciclovías o con infraestructura deficiente.

El diseño está basado en principios de auto-organización de sistemas
complejos. Pruebas de simulador muestran que el sistema funcionará si
suficientes ciclistas lo adoptan.

**Palabras clave: auto-organización, ciclismo, app-movil, seguridad, masa-crítica**

## 1. Introducción

Consecuencias indirectas de una adopción más amplia de la bicicleta
como medio de transporte son: estilos de vida más saludables, y
reducción de emisiones tóxicas. (zero, cual?)

Viajar seguras es un requisito para el uso cotidiano de la bicicleta
como medio de transporte. A continuación se propone el diseño de una
aplicación móvil (app) que fomenta la organización espontánea de
convoyes de ciclistas como estrategia para mejorar la seguridad de la
ciclista urbana.

### 1.1. Masa crítica casual

Pedalear en grupo es más seguro y más divertido. (Jacobsen, 2003;
Secretaría de Salud, 2016)

Cuando se comparten vialidades con vehículos motorizados, formar un
convoy hace más visibles a los ciclistas y les facilita afirmar su
derecho a la calle.

Al viajar en compañía es más fácil obtener ayuda en caso de problemas,
por ejemplo una llanta ponchada o algún desperfecto mecánico.

Además puede ser una eficaz estrategia de seguridad: un convoy viaja
protegido de peligros que amenazan a la ciclista solitaria.

Estas y otras ventajas son ampliamente reconocidas por inumerables
organizaciones que cotidianamente se reunen para pedalear en
grupo.

Una forma más politizada del mismo fenómeno es la
demonstración/festival denominada "Masa Crítica", que se organiza
mensualmente en muchas ciudades del mundo. Centenares de ciclistas se
juntan en convoyes gigantes para pedalear libremente por la
ciudad, celebrando la cultura de ciclismo y afirmando su derecho al
uso de las calles (Furness, 2010).

### 1.2. Ruta semicomún

Por otro lado, organizar viajes compartidos en vehículos privados ha
sido por décadas una forma de optimizar la infraestructura de
transporte. Viajeros se coordinaban usando pizarrones antes de que
hubiera internet, después usando páginas web.

En tiempos recientes existen aplicaciones móviles que pueden obtener
la ubicación del usuario desde un satélite. Usando las coordenadas de
los viajeros, un servicio central puede buscar candidatos cercanos con
destinos parecidos y organizar la ruta semi-común para un taxi que los
lleve a todos en un viaje compartido. Esta forma de compartir viajes
es diferente de las previas pues no requiere de ninguna planificación
previa. La ruta semicomún se planifica en el momento en el que el
usuario solicita un viaje. El viaje compartido es mucho más
conveniente por la flexibilidad que permite la cualidad de
planificación *ad-hoc*.

Esta flexibilidad es posible también al compartir un viaje en
bicicleta. Proponemos el diseño de una app que ayude a sociabilizar
viajes compartidos, de modo que tenga lugar la masa crítica de
manera casual. En la siguiente sección se describe cómo funciona.

## 2. Auto-organización de convoyes

Nuestra app se inspira en modelos basados en agentes como el
desarrollado por Craig Reynolds *circa* 1986. La característica
central del modelo de Reynolds es cómo los agentes, que se denominan
*boids* (de bird-oids, pajaroides), forman lo que parecen parvadas.

El modelo implementa un algoritmo de inteligencia distribuida. Cada
*boid* sigue un conjunto de reglas simples, cuya interacción densa
genera una conducta grupal que tiene su propia identidad. Viajando
como parvada, el grupo de *boids* se disgrega momentáneamente cuando
encuentra obstáculos pero se reagrupa con fluidez. Las reglas son:

 - Evitar colisiones con otros *boids*.
 - Ajustar velocidad y orientación al promedio de los *boids* vecinos.
 - Volar lo más cerca posible del centro de la parvada.

La creación de convoyes de ciclistas puede lograrse siguiendo un
conjunto similar de reglas simples, agregando sólo estas:

 - El viaje de la ciclista tiene origen y destino definidos (no es
   azaroso como el de un *boid*)
 - Candidatas para formar convoyes son otras viajeras que están
   suficientemente cerca y cuyos destinos son cercanos a los de la
   ciclista.

Cada viajera se encarga de navegar el terreno, evitar colisiones y
rodear obstáculos. Estos son problemas locales a resolver de manera
independiente. La app provee la información de escala global necesaria
para la formación de convoyes: qué viajeras están cerca, hacia dónde
van, qué tan rápido se desplazan.

Con ayuda de la app en su dispositivo móvil, la percepción de la
ciclista se amplifica y le permite encontrar el centroide del convoy
de ciclistas más cercano que se dirige a donde ella va. Una ciclista
que pedalea hacia ese centroide al cabo alcanzará a otras ciclistas,
así creando o uniéndose al viaje compartido. Como los miembros del
convoy tienen destinos diferentes, en algún punto priorizan su viaje
individual sobre el colectivo, de modo que los convoyes también se
van desintegrando.

Al viajar en grupo, lo que se comparte es el convoy que crean las
participantes. Cada usuario tiene su propio vehículo con el que puede
acercarse a la ruta semicomún. La ruta semicomún no se computa de
manera centralizada, se crea como propiedad emergente de las
interacciones de las usuarios con la aplicación y con el terreno que
van navegando.

Cabe aclarar que los *boids* son agentes dentro de una simulación y
hacen lo que dicta su programa, mientras que los usuarios de la app
que proponemos son seres humanos que no siguen ningún programa pero
que participan de la actividad por los beneficios que de ella
obtienen.

## 3. Modelación Basada en Agentes

La Modelación Basada en Agentes (MBA) es quizá el marco más general
para modelar y simular sistemas complejos. Un MBA es un modelo
computacional que describe una multitud de agentes discretos. Se
programan como participantes de una simulación computacional, donde
los rasgos y la conducta de cada agente se describen de manera
algorítmica, y no de forma puramente matemática. Esto permite modelar
con gran detalle rasgos y atributos complejos, junto con conductas no
triviales. (Sayama, 2015).

Para analizar cómo el uso de este sistema tiene efecto en las rutas
que eligen sus usuarios, y para calibrar los parámetros de su
operación, desarrollamos una simulación basada en MBA. Esta sección
describe la arquitectura del modelo y algunos resultados observados de
hacer simulaciones con él.

### 3.1. Parámetros de la simulación

#### 3.1.1. Altruismos local y remoto

La operación de la app depende del altruismo de sus usuarios, que
están dispuestas a alterar su ruta con tal de compartirla. Este
altruismo se manifiesta en dos parámetros: qué proporción del viaje
están dispuestas a desviarse para alcanzar a ciclistas cercanas, y qué
destinos ajenos se consideran suficientemente cercanos al suyo.

La figura 1 muestra cómo las ciclistas uno y dos (representadas por
flechas, abajo a la izquierda) son excelentes candidatos para formar
un convoy: pedaleando una corta distancia pueden encontrarse y
compartir la mayor parte del viaje (sus destinos están marcados por la
bandera de meta, arriba a la derecha). Mientras que el ciclista tres
(abajo a la derecha) no es un buen candidato, está lejos y su viaje lo
lleva en la dirección opuesta (su meta está arriba a la izquierda).

Los dos círculos son parámetros de cada agente. Uno en torno a la
ubicación actual de la ciclista: altruismo local. El otro en torno al
destino final del viaje que ha programado: altruismo remoto.

Los radios de estos círculos están dados como una proporción de la
longitud del viaje programado. Si la ciclista se propone pedalear
10km, un altruismo local de 0.1 quiere decir que está dispuesta a
alcanzar caravanas que estén a 1km a la redonda de su ubicación
actual, un altruismo remoto de 0.2 quiere decir que está dispuesta a
seguir una caravana aunque la aleje hasta 2km de su destino
final. Como estos valores son proporciones de la longitud del viaje,
es muy probable que encuentre caravanas si es un viaje largo, y es muy
improbable que encuentre caravanas en los últimos 100 metros.

#### 3.1.2. Densidad

Densidad se refiere a cuántos viajes ocurren por unidad de área
durante un lapso. Es posible configurar este parámetro de la
simulación para asemejarse a datos reportados por censos. Por ejemplo
el Conteo Ciclista 2013 reporta 4000 ciclistas en un área de 5km
durante medio día (conteo 2013).

Esta unidad se puede normalizar a ciclistas por hora por kilómetro
cuadrado.

### 3.2. Resultados

Simulamos cientos de viajes de diversas longitudes, en diferentes
redes de calles y para tres densidades: 50, 100 y 200 ciclistas/h/km2.
Se observa el fenómeno esperado: emergen convoyes de ciclistas.

En estos hipervínculos hemos publicado animaciones de la simulación en
curso, puede verse claramente cómo se forman y desintegran convoyes,
al ir compartiendo sus viajes los agentes.

• <https://archive.org/details/ride_050bikes_cdneza>

• <https://archive.org/details/ride_100bikes_xochimilco>

• <https://archive.org/details/ride_100bikes_uacm>

• <https://archive.org/details/ride_200bikes_cdneza>

#### 3.2.1. Sensibilidad a la densidad

La figura 2 muestra la distribución de tamaños de convoyes para las
densidades simuladas. Las filas agrupan histogramas de tamaños de
convoy cada 200 pasos de la simulación, las columnas agrupan las tres
diferentes densidades. Puede verse que al paso del tiempo van
formándose más convoyes, y que son cada vez más grandes. En todas las
densidades la mayor parte del tiempo la mayoría de los agentes viajan
acompañados. ¡Con una densidad de 200 ciclistas por kilómetro cuadrado
por hora se forman caravanas de hasta 15 ciclistas!


## 4. Diseño de la app móvil

Para traducir el sistema simulado en una aplicación móvil usable por
personas, elegimos un diseño que consta de dos componentes que hacen
partes complementarias: servidor y cliente.

El servidor es un programa que se ejecuta en una computadora pública,
acopia ubicaciones de los usuarios, filtra de acuerdo con los
criterios antes explicados, y devuelve ubicaciones de centroides de
convoyes.

El cliente es un programa que se ejecuta en el dispositivo móvil del
usuario, una app. Se encarga de reportar periódicamente su ubicación
al servidor y de desplegar una especie de brújula que guíe al usuario
hacia otras ciclistas y hacia su destino, con base en las respuestas
del servidor.

La distinción es importante pues este proyecto planeta un diseño para
todo el sistema y propone una implementación para el servidor, que es
un componente relativamente simple, pero deja fuera la implementación
del cliente por razones que se aclaran en la siguiente sección.

### 4.1. Cliente

Una sesión típica del usuario de la aplicación ocurre en el cliente,
así: la ciclista abre la aplicación en su móvil y elige su destino en
un mapa. Se muestran: a) una flecha que apunta hacia el destino
elegido. La ciclista puede navegar el terreno improvisando su ruta,
sin tener que ir alerta a direcciones de qué calles tomar. Mientras
siga la dirección de la flecha se está acercando a su destino y
llegará por una o por otra calle. b) Una segunda flecha que apunta
hacia el convoy más cercana dirigida aproximadamente hacia el
destino final de la ciclista. Usando el tamaño y el color de la flecha,
con algunos letreros adicionales, la aplicación móvil informa a la
ciclista del tamaño y velocidad del convoy, y de cuán cerca
está. Pedaleando en la dirección de la flecha que apunta hacia el
convoy, al cabo se unirá a él.

La figura 3 muestra una maqueta del cliente ejecutándose en un
dispositivo móvil. En ese ejemplo la flecha más clara dice que hay un
convoy compatible con el viaje de la usuario, a 1.2 kilómetros hacia
el norte. En la esquina superior izquierda una leyenda abunda: son
siete ciclistas pedaleando juntas a una velocidad promedio aproximada
de 9 km/h. La flecha más oscura está marcada con una D y dice que
yendo al noroeste se llegará al **D**estino, que está a 8.3 kilómetros
de distancia. Una leyenda abajo a la derecha muestra la velocidad
promedio de la usuario, obtenida del GPS. Usando ambas flechas, cada
ciclista decide si unirse (por que está cerca, por que puede
alcanzarla), cuánto permanecer (por que va hacia donde ella va) y
cuándo abandonar un convoy (por que seguirlo aleja a la ciclista de su
destino final).

#### 4.1.1. Retos del desarrollo del cliente

El desarrollo de software para dispositivos móviles presenta retos
considerables: existen incompatibilidades entre las diferentes
plataformas y muchas veces dentro de las mismas plataformas. Cada
plataforma tiene sus propios estándares, interfaces programáticas,
herramientas y lenguajes de programación. Las diferencias en
capacidades de los dispositivos, e.g. poder del procesador o tamaño de
la pantalla, dificultan el desarrollo de aplicaciones
compatibles. Otros obstáculos son: la rapidez con la que cambian los
estándares y la falta de marcos para pruebas
automatizadas. (#joorabchi2013real)

Estos retos deben enfrentarse con una estrategia adecuada, pues el
funcionamiento de todo el sistema depende de la adopción amplia de la
aplicación móvil. Será necesario soportar el ciclo de vida de
desarrollo de la aplicación (i.e. análisis de requerimientos, diseño,
implementación, pruebas y mantenimiento). Incluso es probable que sean
necesarias campañas de mercadeo. [#inukollu2014factors]

Por lo tanto el desarrollo de la aplicación móvil se considera un
proyecto aparte. Este proyecto se centra en el desarrollo del modelo
bajo el cuál opera todo el sistema. Este modelo está desarrollado de
manera tal que permite crear las simulaciones y, con pocas
modificaciones, ser el núcleo del servidor con el que el sistema podrá
operar en la realidad.

El proyecto de desarrollo del cliente está en etapa de prototipo. Es
software libre, abierto a colaboracion. Más información acá:
<https://social-cycling.gitlab.io/>

### 4.2. Servidor

A través de internet, el servidor recibe ubicaciones actuales y
destinos finales de todos los usuarios. No genera ni sugiere rutas,
sólo devuelve dos direcciones: la que apunta desde la ubicación actual
hacia el destino final y la que apunta desde la ubicación actual hacia
el centroide de los candidatos a formar un convoy. Si no hay
candidatos: sólo devuelve una dirección, la que apunta hacia el
destino final. El cliente interroga al servidor contínuamente de modo
que la dirección hacia el destino final se adapta conforme evoluciona
el viaje, y la dirección hacia el convoy más cercano cambia según la
disponibilidad de otras ciclistas altruistas con viajes compatibles.

Un rasgo importante del servidor es que implementa con muy pocos
cambios el mismo modelo que se usa para las simulaciones descritas en
la sección 3.


## 5. Conclusiones

### 5.1. Probablemente funcionará

Las simulaciones muestran que la cantidad y tamaño de convoyes son una
proporción de la densidad de viajes. Esto sugiere que el tamaño de la
población de ciclistas es un predictor de la cantidad y tamaño de las
caravanas posibles. Así, los datos aportados por censos como «El
conteo ciclista 2013» elaborado por el Instituto de Políticas para el
Transporte y el Desarrollo[#conteo2013] podrían complementarse con
probabilidades de crear masas críticas emergentes para esas ciudades y
barrios.

### 5.2. MBA como Objeto de Frontera

Una aprendizaje valioso de este esfuerzo es que el diseño cuidadoso
del modelo permite su reuso tanto en la simulación como en el
servidor. La estrategia consiste en dividir los componentes de modo
tal que, en un momento dado, puedan sustituirse los agentes
computacionales simulados por personas usando apps.

El comportamiento de la app se analiza de la misma manera que el
comportamiento de las simulaciones, pues se trata esencialmente del
mismo software. Esto representa una ventaja sustancial: el marco de
simulación se vuelve un marco para probar posibles cambios a la
app. 

Así, el MBA se convierte en una representación común del espacio del
problema para las partes interesadas. La discusión acerca de cómo
mejorar el funcionamiento de la app puede ayudarse de el MBA como
objeto de frontera (Star y Griesemer, 1989), usándolo para traducir
conocimiento entre diferentes audiencias.

### 5.2. Normativas vertical y horizontal para la Ciudad Inteligente

Dependiendo de la perspectiva de los involucrados, una normativa puede
describirse como vertical u horizontal (Walravens, 2015). Estas son
perspectivas complementarias, y ambas pueden enfocar nuestro diseño.

Intervenciones verticales a los sistemas de movilidad sólo pueden
provenir del Gobierno. Desde esta perspectiva de Ciudad Inteligente,
hay dos contribuciones posibles:

1. Acopio de datos. Un efecto lateral del uso de esta aplicación es
   que se colectan bitácoras de viajes que detallan por dónde
   circularon los usuarios. Esta información es valiosa para la
   planificación de vías.

2. Integración a más apps y repositorios de datos. Otras tecnologías
   de Ciudad Inteligente podrían interconectarse, por ejemplo las que
   se desarrollan en torno a sistemas de transporte público, para
   articular viajes intermodales.

Proveer infraestructura de movilidad adecuada es la actividad
sustancial de varias organizaciones gubernamentales. Además, existen
diversos acuerdos internacionales dedicados a la mitigación de cambio
climático, a través de la reducción del uso de combustibles fósiles.

Desde la perspectiva horizontal, los cambios y mejoras vienen sólo de
las personas "viviendo" la ciudad. En esta perspectiva, lo que define
la Ciudad Inteligente no es la infraestructura sino las formas en que
los ciudadanos interactúan con los sistemas y entre sí (Walravens,
2015). Este enfoque distribuido es la mayor fortaleza de nuestro
diseño. A través de la infusión de inteligencia se empodera la
ciudadanía. Amplificando su percepción de viajes circundantes y
similares, nuestra app hace más valiosa la toma local de decisiones,
convierte decisiones individuales de transporte en activismo.

