# ABM simulations for different densities and street network topologies

This agent based simulation has been developed to aid in the design of
"[Social Cycling](https://gitlab.com/rgarcia-herrera/social-cycling/)",
a mobile app that helps with the coordination of bike
riders so that flocks are assembled in a simple and spontaneous way.

An easy to follow description of the model is available [here](../abm_comses).

Scripts in this directory were developed to analyze different rider
densities and different street networks. Hundreds of simulations were run
through [HTCondor](http://htcondor.org) in a high throughput computing
cluster. An overview of the pipeline is given [here](code/).

# Results

Results indicate that changing the rules to consider cyclists
(only flock to nearby riders that are aproximately heading to the
user's destination) does not impede the formation of flocks.
Furthermore, flocking is likely to occur with enough users of the
app.

## Animations

Animations of different simulation runs:

 - <https://archive.org/details/ride_050bikes_cdneza>
 - <https://archive.org/details/ride_100bikes_xochimilco>
 - <https://archive.org/details/ride_100bikes_uacm>
 - <https://archive.org/details/ride_200bikes_cdneza>

## Response to density

Twenty simulations are run for densities of 50, 100 and 200 riders per
square kilometer per hour. For each time step population sizes are
counted for three states: 1. Solo riders, 2. riders seeking a flok
and 3. Flocking riders.  The system responds with a larger population
of flocking riders as the density increases. This seems like a
reasonable result: during the small hours of the night when almost no
bike riders are out and about flocks are unlikely, whereas at rush
hour they are almost inevitable, with or without the use of the app.


The following figure shows the dynamics of population size for
each density. Flocks are formed even with the lowest density
after the first three simulated minutes. Beyond 100 cyclists per
hour per square kilometer, there are more flocked riders than
solo riders.

<table>
<tr>
<td>
<img src="../doc/050_uacm.png">
rho=50
</td>
<td>
<img src="../doc/100_uacm.png">
rho=100
</td>
<td>
<img src="../doc/200_uacm.png">
rho=200
</td>
</tr>
</table>

Population sizes for agents in different states for three densities,
from left to right: rho=50, rho=100, rho=200 . Blue curves depict
solo riders, green curves represent the population of agents riding
towards a flock, red curves represent the population size of flocked
agents. Each plot shows 20 repetitions of a simulation run for the
same density.

This other figure shows the distribution of flock
sizes for simulated densities, at lapses of 200 simulation steps.
As time goes by, more flocks emerge and grow. With 200 agents per
square kilometer per hour some flocks are formed with up to 15
members, and more agents ride within a flock than alone.

![flock size distribution](../doc/flock_size_distribution.png)


## Response to street network topology

What are the effects of the shape of the street network? The
orientation of streets is used as a [measure of spatial order](https://dx.doi.org/10.2139/ssrn.3224723).
Four different street networks are used to run simulations.

In the following figure radial histograms show street orientation
frequencies, as a measure of spatial order. Population size of
flocking agents increases with more entropic street networks.
Probably because smaller blocks allow for a more direct route to a
flock, whereas larger blocks require a longer ride just to change
streets.

![terrain topology](../doc/terrain_topology.png)


## Trip duration

While using the app a cyclist has the alternative of deviating
from a more direct route in order to join with other riders. She
may not ride alone, but the trade-off is that her ride will
probably be longer. Each user will decide with different
criteria, however all are part of the distributed intelligence
enabled by the use of the app, from which flocks emerge.

This figure compares the distribution of trip lengths for rides with
and without the app.  Solo trips (blue bars) are shorter, flocked
rides (orange bars) are longer. This longer trip length is due to two
phenomena: 1. Routes required to go through the centroid of a flock
are less direct than routes that head directly towards the agent's
destination 2. In the simulation, flocking agents match their speeds,
which are lower than those of solo riders.

![ride len distribution](../doc/ride_len_dist.png)
