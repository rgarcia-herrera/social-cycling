#!/usr/bin/env python
import argparse
import pickle
import numpy as np
import csv

parser = argparse.ArgumentParser(
    description='export flock distribution from pickle to CSV')

parser.add_argument('--log', type=argparse.FileType('rb'),
                    help='path to pickled ride logs')

parser.add_argument('--csv', type=argparse.FileType('w'), required=True,
                    help='path to place CSV output')

args = parser.parse_args()

l = pickle.load(args.log)

bins = 16

def fs(t):
    """ return bins, flock sizes """
    hist, bin_edges = np.histogram([l[bk]['flock_size'].get(t, 0) for bk in l], range(1,bins))
    return [0, ] + list(hist)


writer = csv.writer(args.csv)

for t in range(0, 1800):
    xs = np.arange(bins - 1)
    writer.writerow([t, ] + fs(t))
