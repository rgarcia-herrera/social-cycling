#!/usr/bin/env python

import csv
import pandas as pd
import matplotlib.pyplot as plt
plt.rc('text', usetex=True)

def csv2df(f):
    rows = []
    reader = csv.reader(f)
    for row in reader:
        rows.append(row[2:])
    df = pd.DataFrame(rows, columns=range(2, 16), dtype=int)
    return df


with open('../../../data/flock_size_dist_050bikes_cu.csv') as f050, \
     open('../../../data/flock_size_dist_100bikes_cu.csv') as f100, \
     open('../../../data/flock_size_dist_200bikes_cu.csv') as f200:

     df050 = csv2df(f050)
     df100 = csv2df(f100)
     df200 = csv2df(f200)


f, axarr = plt.subplots(9, 3,
                        sharey=True,
                        figsize=(11, 8), dpi=200)
plt.setp(axarr.flat, xlabel='flock size', ylabel='frequency')


step = 200
x = 0
for t in range(0, 1800, step):
    y = 0
    for df in (df050, df100, df200):
        means = df[t:t+step].mean()
        errors = df[t:t+step].std()
        means.plot.bar(yerr=errors, ax=axarr[x, y], width=0.9)
        axarr[x, y].set_ylim(0, 150)
        y += 1
    x += 1


axarr[0,0].set_title(r'$\rho=50$')
axarr[0,1].set_title(r'$\rho=100$')
axarr[0,2].set_title(r'$\rho=200$')

pad = 4

rows = [r'$%s\leq t\leq %s$' % (t, t+step) for t in range(0, 1800, step)]
for ax, row in zip(axarr[:, 0], rows):
    ax.annotate(row, xy=(0, 0.1), xytext=(-ax.yaxis.labelpad - pad, 0),
                xycoords=ax.yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center')

f.subplots_adjust(hspace=0.15)
f.subplots_adjust(left=0.25)
#plt.tick_params(axis='x', which='major', labelsize=9, rotation=60)

plt.savefig('../../../images/flock_size_distribution.png', bbox_inches='tight')
