import pickle
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import models

plan = pickle.load(open('../../../data/plan_200bikes_bbox_uacm_graph.pickle', 'rb'))
log = pickle.load(open('../../../data/log_23_plan_200bikes_bbox_uacm_graph.pickle', 'rb'))


solo = [len(a.route)
            - a.t
            for a in plan]

flocking = [max(log[k]['status'].keys())
                - min(log[k]['status'].keys())
                for k in log]

dfU = pd.DataFrame({'solo': np.histogram(solo, bins=range(200, 2400, 100))[0],
                    'flocking': np.histogram(flocking, bins=range(200, 2400, 100))[0]},
                       columns=['solo', 'flocking'])

ax = dfU.plot.bar(width=1.0, figsize=(6,6))
ax.set_xticklabels(range(200, 2400, 100))
ax.set_xlabel("ride length")
ax.set_ylabel("frequency")

plt.savefig('../../../doc/ride_len_dist.png')
