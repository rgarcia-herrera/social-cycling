#!/usr/bin/env python

import argparse
from multiprocessing.dummy import Pool as ThreadPool
import utm
import pickle
from os import path
from os import makedirs
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import osmnx as ox


parser = argparse.ArgumentParser(
    description='create a png frame for each time step in a ride')

parser.add_argument('--rides', type=argparse.FileType('r'), required=True,
                    help='path to pickled rides log')

parser.add_argument('--osm_graph', type=argparse.FileType('r'), required=True,
                    help='path to pickled osmnx graph to use as background')

parser.add_argument('--threads', default=4, type=int,
                    help='number of parallel computing threads ')

parser.add_argument('--init_step', default=0, type=int,
                    help='start plotting at this simulation step')

parser.add_argument('--end_step', default=100, type=int,
                    help='plot until this simulation step')

parser.add_argument('--outdir', default=".",
                    help='dir name to output frames, will be created')

parser.add_argument('--arrows', default=None,
                    help='draw arrows for destination and flock headings, default=no')

parser.add_argument('--force', default=False,
                    help='force overwrite of png output')


args = parser.parse_args()

if not path.exists(args.outdir):
    makedirs(args.outdir)

print "loading osm-graph"
gp = pickle.load(args.osm_graph)

print "loading rides log"
r = pickle.load(args.rides)


def arrow(point, theta, R, ax, color='firebrick', alpha=1.0):
    vertices = [(0, R), (R/2.0, R), (R/4.0, 0)]
    polygon = patches.Polygon(vertices, color=color, alpha=alpha)
    r = matplotlib.transforms.Affine2D().rotate_deg_around(R/4.0, 0, theta)
    t = matplotlib.transforms.Affine2D().translate(point[0], point[1])
    tra = r + t + ax.transData
    polygon.set_transform(tra)
    return polygon


def render(t):
    filename = "%s/f_%06d.png" % (args.outdir, t)
    if path.isfile(filename) and not args.force:
        print "cowardly refusing to overwrite %s, use --force True" % filename
        return None

    fig, ax = ox.plot_graph(gp,
                            node_size=0,
                            edge_alpha=0.3,
                            edge_color='indigo',
                            dpi=400,
                            fig_height=15)

    for b in r:
        if t not in r[b]['points']:
            continue

        p = r[b]['points'][t]
        point = utm.from_latlon(*(float(l) for l in p.to_string()))

        s = r[b]['status'][t]
        if s == 'solo':
            color = 'coral'
            R = 15
            alpha = 0.8

        elif s == 'flocking':
            color = 'mediumorchid'
            R = 15
            alpha = 0.8
            if args.arrows is not None:
                ax.add_patch(arrow(point,
                                   r[b]['flock_heading'][t], R=120,
                                   color='red', alpha=0.8, ax=ax))

        elif s == 'flocked':
            color = 'deepskyblue'
            R = 32
            alpha = 0.4
            if args.arrows is not None:
                ax.add_patch(arrow(point, r[b]['dest_heading'][t],
                                   R=111,
                                   color='green', alpha=0.7, ax=ax))

        ax.add_artist(plt.Circle(point, R, color=color, alpha=alpha))

    fig.savefig(filename)
    plt.close(fig)
    print("rendered %s" % filename)


print "starting frame rendering"
if args.threads > 1:
    pool = ThreadPool(args.threads)

    pool.map(render,
             range(args.init_step, args.end_step))
else:
    map(render,
        range(args.init_step, args.end_step))

# render to mp4 thusly
# ffmpeg -r 15 -start_number 000 -i f_%03d.png ride.mp4
# convert -loop 0 *png ride.gif
