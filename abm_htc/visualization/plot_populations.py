#!/usr/bin/env python

import argparse
import numpy as np
import pickle
import matplotlib; matplotlib.use('Agg')
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(
    description='plot bike populations by status during rides')

parser.add_argument('--log', type=argparse.FileType('rb'), nargs="+",
                    help='path to pickled ride logs')

parser.add_argument('--png', type=argparse.FileType('w'), required=True,
                    help='path to place png output')

parser.add_argument('--t0', default=0, type=int,
                    help='start plotting at this simulation step')

parser.add_argument('--tn', default=100, type=int,
                    help='plot until this simulation step')


args = parser.parse_args()

assert args.tn > args.t0


def plot_ride(pickled_log):
    r = pickle.load(pickled_log)

    solo = np.zeros(args.tn)
    flocked = np.zeros(args.tn)
    flocking = np.zeros(args.tn)

    for t in range(args.t0, args.tn):
        for b in r:
            if r[b]['status'].get(t, '') == 'solo':
                solo[t] += 1
            elif r[b]['status'].get(t, '') == 'flocking':
                flocking[t] += 1
            elif r[b]['status'].get(t, '') == 'flocked':
                flocked[t] += 1

    plt.plot(range(args.tn - args.t0),
             solo[args.t0:],
             color='dodgerblue',
             linewidth=1.0/len(args.log))
    plt.plot(range(args.tn - args.t0),
             flocking[args.t0:],
             color='green',
             linewidth=1.0/len(args.log))
    plt.plot(range(args.tn - args.t0),
             flocked[args.t0:],
             color='coral',
             linewidth=1.0/len(args.log))


fig = plt.figure(dpi=130, figsize=(6, 4))

for log in args.log:
    plot_ride(log)

ax = fig.gca()
#ax.set_aspect('equal')
ax.set_xlim((0, (args.tn - args.t0)))
ax.set_ylim((0, 450))

plt.setp(ax, xlabel='simulation steps', ylabel='population size')

fig.savefig(args.png)
