import numpy as np


with open('../plots/table.html', 'w') as f:
    f.write("<table>\n")
    for p in np.linspace(0.1, 0.4, 10):
        f.write("<tr>\n")
        for d in np.linspace(0.1, 0.4, 10):
            f.write("<td><img width='100px' src='plot_%0.2f_%0.2f.png'></td>\n" % (p, d))
        f.write("</tr>\n")
    f.write("</table>\n")


for p in np.linspace(0.1, 0.4, 10):
    for d in np.linspace(0.1, 0.4, 10):
        print "python plot_ride.py --rides log/ride_100b_%0.2f_%0.2f_*pickle --png ../plots/plot_%0.2f_%0.2f.png --point %s --dest %s&" % (p, d, p, d, p, d)
