#!/bin/bash

./plot_populations.py --log ../../../data/log_*_plan_050*uacm*.pickle --t0 0 --tn 1800 --png ../../../figs/050_uacm.png &
./plot_populations.py --log ../../../data/log_*_plan_050*cu*.pickle --t0 0 --tn 1800 --png ../../../figs/050_cu.png &
./plot_populations.py --log ../../../data/log_*_plan_050*xochimilco*.pickle --t0 0 --tn 1800 --png ../../../figs/050_xochimilco.png &
./plot_populations.py --log ../../../data/log_*_plan_050*cdneza*.pickle --t0 0 --tn 1800 --png ../../../figs/050_cdneza.png &
./plot_populations.py --log ../../../data/log_*_plan_100*uacm*.pickle --t0 0 --tn 1800 --png ../../../figs/100_uacm.png &
./plot_populations.py --log ../../../data/log_*_plan_100*cu*.pickle --t0 0 --tn 1800 --png ../../../figs/100_cu.png &
./plot_populations.py --log ../../../data/log_*_plan_100*xochimilco*.pickle --t0 0 --tn 1800 --png ../../../figs/100_xochimilco.png &
./plot_populations.py --log ../../../data/log_*_plan_100*cdneza*.pickle --t0 0 --tn 1800 --png ../../../figs/100_cdneza.png &
./plot_populations.py --log ../../../data/log_*_plan_200*uacm*.pickle --t0 0 --tn 1800 --png ../../../figs/200_uacm.png &
./plot_populations.py --log ../../../data/log_*_plan_200*cu*.pickle --t0 0 --tn 1800 --png ../../../figs/200_cu.png &
./plot_populations.py --log ../../../data/log_*_plan_200*xochimilco*.pickle --t0 0 --tn 1800 --png ../../../figs/200_xochimilco.png &
./plot_populations.py --log ../../../data/log_*_plan_200*cdneza*.pickle --t0 0 --tn 1800 --png ../../../figs/200_cdneza.png

sleep 120
