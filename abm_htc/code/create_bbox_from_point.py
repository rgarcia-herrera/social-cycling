#!/usr/bin/env python


import argparse
from LatLon import LatLon, Latitude, Longitude
import json

parser = argparse.ArgumentParser(
    description='json encode bounding box around point, e.g.: 19.43487 -99.13210 1.0 cdmx_km0.json')

parser.add_argument('lat', type=float,
                    help='latitude')

parser.add_argument('lon', type=float,
                    help='longitude')

parser.add_argument('dist', type=float,
                    help='len of side of square, in km, default=1.0')

parser.add_argument('json', type=argparse.FileType('w'),
                    help='path to output json description of bbox')


args = parser.parse_args()


k0 = LatLon(Latitude(args.lat), Longitude(args.lon))


def latitudinal(delta=0.000001, distance=0.5):
    h = args.lat
    b = LatLon(Latitude(h), Longitude(args.lon))
    while k0.distance(b) < distance:
        h += delta
        b = LatLon(Latitude(h), Longitude(args.lon))
    return h


def longitudinal(delta=0.000001, distance=0.5):
    v = args.lon
    b = LatLon(Latitude(args.lat), Longitude(v))
    while k0.distance(b) < distance:
        v += delta
        b = LatLon(Latitude(args.lat), Longitude(v))
    return v


bbox = {"ne_lat": latitudinal(delta=0.0000001, distance=args.dist/2.0),
        "ne_lng": longitudinal(delta=0.0000001, distance=args.dist/2.0),
        "sw_lat": latitudinal(delta=-0.0000001, distance=args.dist/2.0),
        "sw_lng": longitudinal(delta=-0.0000001, distance=args.dist/2.0)}

json.dump(bbox, args.json)
