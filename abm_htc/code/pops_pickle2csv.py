#!/usr/bin/env python

import csv
import numpy as np
import argparse
import pickle

parser = argparse.ArgumentParser(
    description='export bike populations by status to a csv file')

parser.add_argument('--log', type=argparse.FileType('rb'), required=True,
                    help='path to pickled ride logs')

parser.add_argument('--csv', type=argparse.FileType('w'), required=True,
                    help='path to place output')

args = parser.parse_args()

r = pickle.load(args.log)
t0 = 0
tn = max([max(b['status'].keys()) for b in r.values()])

solo = np.zeros(tn)
flocked = np.zeros(tn)
flocking = np.zeros(tn)

for t in range(t0, tn):
    for b in r:
        if r[b]['status'].get(t, '') == 'solo':
            solo[t] += 1
        elif r[b]['status'].get(t, '') == 'flocking':
            flocking[t] += 1
        elif r[b]['status'].get(t, '') == 'flocked':
            flocked[t] += 1

writer = csv.writer(args.csv)

for t in range(t0, tn):
    writer.writerow([t, solo[t], flocking[t], flocked[t]])
