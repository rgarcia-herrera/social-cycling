# Submit files

This directory contains submit files for running density simulations
through an [HT-Condor](http://research.cs.wisc.edu/htcondor/) queue.

[This file](density_sim.dag) specifies the run order and hierarchy.