# Density simulations

This directory contains scripts to configure and run simulations of
different population densities.

![pipeline](simulation_pipeline.png)
