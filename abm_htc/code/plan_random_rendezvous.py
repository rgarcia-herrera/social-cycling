import models
import argparse
import pickle
import json
import numpy as np
from multiprocessing.dummy import Pool as ThreadPool

parser = argparse.ArgumentParser(
    description='plan rides to a rendezvous')

parser.add_argument('--N', default=100, type=int,
                    help='number of rides')

parser.add_argument('--steps', default=500, type=int,
                    help='number of simulation steps')

parser.add_argument('--bbox', type=argparse.FileType('r'), required=True,
                    help='bounding box in json file')

parser.add_argument('--rendezvous_lat', required=True, type=float,
                    help='place to end rides')

parser.add_argument('--rendezvous_lon', required=True, type=float,
                    help='place to end rides')

parser.add_argument('--min_len', default=5.0, type=float,
                    help='minimum ride length in km')

parser.add_argument('--max_len', default=10.0, type=float,
                    help='maximum ride length in km')

parser.add_argument('--speed', default=3.0, type=float,
                    help='speed in meters per second, default=3.0')

parser.add_argument('--threads', default=4, type=int,
                    help='number of parallel computing threads')

parser.add_argument('--pickle', type=argparse.FileType('w'), required=True,
                    help='path to output pickled planned rides')

args = parser.parse_args()

bbox = json.load(args.bbox)


def new_bike(n):
    """ return new bike instance """
    b = models.Agent()
    b.speed = args.speed

    b.t = np.random.choice(args.steps)

    b.random_rendezvous(ne_lat=bbox['ne_lat'],
                        ne_lng=bbox['ne_lng'],
                        sw_lat=bbox['sw_lat'],
                        sw_lng=bbox['sw_lng'],
                        rendezvous_lat=args.rendezvous_lat,
                        rendezvous_lon=args.rendezvous_lon,
                        min_len=args.min_len,
                        max_len=args.max_len)
    return b


pool = ThreadPool(args.threads)

all_bikes = pool.map(new_bike, range(args.N))

pickle.dump(all_bikes, args.pickle)
