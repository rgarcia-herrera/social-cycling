#!/usr/bin/env python

import argparse
import pickle
from copy import copy

parser = argparse.ArgumentParser(
    description='simulate N rides!')

parser.add_argument('--threads', default=1, type=int,
                    help='how many threads to use in pool')

parser.add_argument('--steps', default=500, type=int,
                    help='simulation time steps')

parser.add_argument('--point', default=0.1, type=float,
                    help='point altruism: '
                         + 'radius around point to seek flockers. '
                         + 'Radius is a ratio of ride length.')

parser.add_argument('--dest', default=0.2, type=float,
                    help='destination altruism: '
                         + 'radius around destination to seek flockers. '
                         + 'Radius is a ratio of ride length.')

parser.add_argument('--plan', type=argparse.FileType('r'), required=True,
                    help='path to pickled ride plan')

parser.add_argument('--log', type=argparse.FileType('w'), required=True,
                    help='path to output pickled logfile')

args = parser.parse_args()



# init bikes
all_bikes = pickle.load(args.plan)
for bk in all_bikes:
    bk.point_altruism = args.point
    bk.dest_altruism = args.dest
    bk.router.profile = "safety"
    bk.speed = 4


# initialize empty log for loaded bikes
bike_log = {id(b): {'status': {},
                    'points': {},
                    'flock_size': {},
                    'speed': {},
                    'init_route': copy(b.route)
                    } for b in all_bikes}


# ride function
def ride(b):
    flock_size = len(b.flocking(riding))
    if flock_size > 0:
        b.status = "flocked"
        b.speed = 3.5
        b.update_route()

    bike_log[id(b)]['status'][t] = b.status
    bike_log[id(b)]['points'][t] = b.point()
    bike_log[id(b)]['flock_size'][t] = flock_size
    bike_log[id(b)]['speed'][t] = b.speed

    b.step()



for t in range(args.steps):
    riding = [bk for bk in all_bikes if not bk.got_there() and t >= bk.t]

    for bk in riding:
        if t % 10 == 0:
            bk.flock(riding)

        ride(bk)



pickle.dump(bike_log, args.log)
