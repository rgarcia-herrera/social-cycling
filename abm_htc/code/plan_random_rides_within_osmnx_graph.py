#!/usr/bin/env python


from LatLon import LatLon, Latitude, Longitude
from router import Router
import models
import argparse
import pickle
import numpy as np
import osmnx as nx


parser = argparse.ArgumentParser(
    description='plan rides within map-graph')

parser.add_argument('--N', default=100, type=int,
                    help='ride population')

parser.add_argument('--steps', default=500, type=int,
                    help='number of simulation steps')

parser.add_argument('--osm_graph', type=argparse.FileType('r'), required=True,
                    help='path to pickled osmnx graph')

parser.add_argument('--min_len', default=5.0, type=float,
                    help='minimum ride length in km')

parser.add_argument('--max_len', default=10.0, type=float,
                    help='maximum ride length in km')

parser.add_argument('--speed', default=3.0, type=float,
                    help='speed in meters per second, default=3.0')

parser.add_argument('--threads', default=4, type=int,
                    help='number of parallel computing threads')

parser.add_argument('--pickle', type=argparse.FileType('w'), required=True,
                    help='path to output pickled planned rides')

args = parser.parse_args()

g = pickle.load(args.osm_graph)

rtr = Router(profile="safety")


def new_bike(n):
    """ return new bike instance """
    b = models.Agent()
    b.speed = args.speed
    b.t = np.random.choice(args.steps)

    while True:
        s, t = np.random.choice(g.nodes, 2)
        A = LatLon(Latitude(g.node[s]['lat']),
                Longitude(g.node[s]['lon']))
        B = LatLon(Latitude(g.node[t]['lat']),
                Longitude(g.node[t]['lon']))

        if A.distance(B) >= args.min_len and A.distance(B) <= args.max_len:
            b.set_point(A)
            b.set_destination(B)

            route = rtr.get_route(points=[b.point(), b.destination()],
                                  speed=b.speed)
            if route:
                b.route = route
                break


    return b


all_bikes = []

for i in range(args.N):
    all_bikes.append(new_bike(i))

pickle.dump(all_bikes, args.pickle)
