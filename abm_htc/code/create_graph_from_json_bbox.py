#!/usr/bin/env python

import argparse
import pickle
import json
import osmnx as ox

parser = argparse.ArgumentParser(
    description='create pickled NX graph from Open Street Map bounding box')

parser.add_argument('--bbox', type=argparse.FileType('r'), required=True,
                    help='bounding box in json file')

parser.add_argument('--graph', type=argparse.FileType('wb'), required=True,
                    help='path to output pickled osm-nx graph of bbox')

args = parser.parse_args()

bbox = json.load(args.bbox)

# download and save OSM-NX graph for later plotting
G = ox.graph_from_bbox(bbox['ne_lat'], bbox['sw_lat'],
                       bbox['ne_lng'], bbox['sw_lng'],
                       network_type='bike',
                       simplify=False,
                       clean_periphery=False)

graph_projected = ox.project_graph(G)

pickle.dump(graph_projected, args.graph)
