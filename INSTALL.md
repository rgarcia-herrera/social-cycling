# Instalación

1. Descargar o clonar este repositorio.


```
$ git clone https://gitlab.com/rgarcia-herrera/social-cycling.git
[...] # se clona el repo
$ cd social-cycling
~/social-cycling$

```

## Ambiente de Python

Las bibliotecas necesarias para correr las simulaciones están
especificadas en el [archivo de requerimientos](abm/requirements.txt).
Se recomienda instalarlas dentro de un [ambiente
virtual](http://virtualenv.org).


```
# crear el ambiente
~/social-cycling$ virtualenv venv
# activar el ambiente
~/social-cycling$ source venv/bin/activate
(venv)~/social-cycling$   # ambiente activado

(venv)~/social-cycling$ cd abm
# Instalar bibliotecas de Python
(venv)~/social-cycling/abm$ $ pip install -r requirements.txt
[...]  # se instalan bibliotecas de python.
```



## Servidor BRouter

Para navegar las calles los agentes usan BRouter: un servidor de rutas
que devuelve en GeoJSON una lista de ubicaciones que marcan la
trayectoria de un viaje, desde un origen hasta un destino.

A continuación instrucciones de cómo instalar y arrancar un servidor
local de BRouter.

1. Descargar brouter.

Recomiendo descargar e instalar brouter dentro del directorio de
social-cycling.

```
$ cd social-cycling/brouter
~/social-cycling/brouter$ wget http://brouter.de/brouter_bin/brouter_1_4_9.zip
~/social-cycling/brouter$ unzip brouter_1_4_9.zip
```

2. Descargar tablas de ruteo. Con ese script se bajan *todas*, son 4.5 GB.

```
~/social-cycling$ cd brouter/segments4
~/social-cycling/brouter/segments4$ ./get_segments.sh
# [...]  se descargan 4.5 gigas de datos de segmentos
```

3. Arrancar servidor

```
$ cd ~/social-cycling/brouter/standalone
~/social-cycling/brouter/standalone$ ./server.sh
BRouter 1.4.9 / 24092017

```


Con el servidor de rutas escuchando estamos listos para correr
simulaciones.
