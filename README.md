# Social Cycling: emergent critical mass through a mobile app

Social Cycling is a mobile application that lets bicycle riders flock
with other riders while commuting, and benefit from the emergent
[critical mass](https://en.wikipedia.org/wiki/Critical_Mass_(cycling)).

This repository documents the design of the app, including an [agent based model](abm_comses/) developed to simulate its use.

## Sharing a bike ride with Social Cycling

Riding a bike among other cyclists is [safer](http://dx.doi.org/10.1136/ip.9.3.205rep) and more fun.
It has many advantages for riders, for example:

 - Better visibility.
 - It is easier for a group to assert its right to occupy a whole
   lane.
 - Riders can readily rely on the kindness of strangers in their
   group, should trouble arise, e.g. mechanical failure of bikes,
   flat tires.

Social Cycling is a mobile app that helps with the coordination
of bike riders so that flocks are assembled in a simple and
spontaneous way.

### Boids

The emergence of flocks has been studied by simulations such as
[Craig Raynolds' Boids](https://en.wikipedia.org/wiki/Boids) It is essentialy
a model of distributed intelligence in which every agent chooses its
own route. In Raynolds' simulation each boid is an independent actor
that navigates acording to its perception of the local environment and
a set of rules. Dense interaction of these simple behaviors by
individuals results in the collective coordination of boid
flocks. These flocks do not rely on a central control, but retain
their group identity even while they momentarily disperse to avoid
obstacles, as is shown in this figure:

<img src="doc/flocking_around_19.gif" width="55%">


Boids follow these rules:

 1. Avoid collisions with other boids.
 2. Match speed and heading of the flock by adopting an average of
	surrounding boids.
 3. Keep close to what it percieves as the center of the flock.

####  Cyclistoids

The spontaneous creation of cyclist flocks may be achieved by similar
mechanisms. Real life cyclists are not really bound by rules like the
ones described for boids, but the advantages of riding in a group are
likely to motivate individuals. Parts of the collective behavior
can be delegated to actual bicycle riders, e.g. they take care of
navigating the terrain, avoiding obstacles and collisions, all
problems to be dealt with localy. The mobile application provides
system-wide information so that the distributed intelligence of flock
formation can take place by getting close to other riders, matching
heading and speed. The sensorium of a cyclist would be augmented
through the usage of the app in her mobile device, helping her find:

 1. Nearby bicycle riders with destinations close to her own.
 2. The centroid of those riders.

A cyclist riding towards the centroid of a perceived flock will
eventually catch up with other riders going in her own direction,
thereby joining or creating a flock. However, it is reasonable to
suppose that riders will prioritize their own destination, so
flocks may be disintegrated as riders part.

The operation of the Social Cycling app depends on its users'
altruism, by which they are willing to alter their route in order
to share it. This altruism is manifested in two parameters: what
ratio of their trip they are willing to ride to join flocks near
their current location, and what destinations they consider close
enough to their own.

In the following figure arrows represent bike riders, same colored
flags their destinations. The green circle shows the radius of red's
local altruism, i.e. what distance surrounding her she's willing to
cover in order to join with others. The red circle shows red's
destination altruism, i.e. what distance surrounding her destination
is close enough to include in her trip.

Red and blue cyclists are great candidates for forming a flock: by
riding a short distance they can meet and share most of the trip. The
pink cyclist however is not a good match, he is far away and riding in
the opposite direction.

<img src="doc/altruisms.png" width="50%">


## Design and implementation

The application design is client-server. Functionality is split into
two programs that cooperate: the server program is a central component
that gathers clients' locations into a database, and using the model
serves flock centroids back to clients seeking a flock. The client
program is meant to run on mobile devices of cyclists, maybe mounted
on their handlebars. It uploads their locations to the server and
displays a special compass that cyclists use to join other cyclists.

### Client

A tipical session might be: user opens the app in her mobile
device. She choses her destination in a map. Two arrows are shown. One
points to her destination, if she heads in the direction pointed to by
this arrow she will eventually get there, by whatever route she
improvises. A second arrow points towards the closest flock which is
heading to a place nearby the user's destination. Using the size and
color of both arrows along with some text the app informs the user
about the size, speed and closeness of the flock. If she rides in the
direction pointed to by this second arrow she will eventually join
other riders.

The following figure is a prototype of the client app running in a
mobile device. The arrows should be properly oriented using the
device's magnetometer. In this example a green arrow points towards a
flock with a trip similar to the user's, 1.2 kilometers away due
north. The top left part of the screen further shows that it is made
up of seven riders going at around 9 km/h. There is also a red arrow
which points north-east towards a destination 8.3 km away. Bellow this
red arrow the user can read her own average speed, computed using
times and distances from her updates. By using both arrows each user
decides if she wants to join (because it is near), how long to belong
(because they are heading in the same direction) and when to leave a
flock (because she must head to her own destination).

<img src="doc/client_mockup.png" width="50%">

Software development for mobile devices faces [considerable challenges](https://doi.org/10.1109/ESEM.2013.9):
e. g. there are many different incompatible platforms, often with
incompatibilities within the same platform and each platform has its
own standards, interfaces, tools and programming
languages. Development is further made difficult by differences in
devices' specifications, like CPU speed and screen size. Other
obstacles include: the speed at which standards change and the lack of
standardized testing platforms.

To achieve mass adoption of the app these challenges must be met with
an appropiate strategy which supports [the whole development
life-cycle](https://arxiv.org/abs/1410.4537) (requirements analylsis,
design, implementation, tests, maintenance). Therefore the development
of the mobile app must spin off into a different project. This project
then focuses on the development of the model at the core of the app.
This model is developed in such a way that it can run the server side
of the real-life system or, with little modification, the different
simulations discussed below.


### Flock Server

The server receives locations and destinations of users. It does
not compute routes, it just returns two headings: one towards the
user's destination, one towards the centroid of the nearest
flock. If there are no flocks nearby, it will just return the
heading towards the user's destination. The client will query the
server periodically so that the heading towards the user's
destination will be updated as the trip goes on, and the heading
towards nearby flocks will change according to availability of
concurrent users with similar trips.

Comunication between client and server is arranged as a
webservice. The interface for the client consists of two URLs for
registering a trip and updating its location.


**Registering a user session:**

The synopsis of registering a trip is shown in the following code,
coordinates of current location and destination are sent, server
returns a **bike_id** which identifies the user for further
transactions.


	GET /register/?dest_lat=${dlat}&dest_lon=${dlon}
	Response Status 200 {'bike_id': 5263}

**Client location update:**

The synopsis for updates is shown in the following code: ${bike_id} is
the key obtained at session start, it is sent with the current user's
coordinates represented by the variables ${lat} and ${lon}, server
returns two headings: **dest_heading** and **flock_heading** along with data
about size, speed and distance to nearest flock, destination and
user's speed.


	GET /update/${bike_id}/?lat=${lat}&lon=${lon}
	Response Status 200 {'flock_heading': 34.24,
                         'Flock_distance': 0.43,
                         'flock_avg_speed': 10.2,
                         'flock_size': 4,
                         'dest_heading': 33.11, 'dest_distance': 6,
                         'speed': 9.4}

Entries in the server's database are time-stamped so that only
current data is returned.

Check out the [Flock Server](flock_server/).



# Agent Based Modeling

To aid the design of the app an Agent Based Model was developed.
Here is an easy to follow [Jupyter
Notebook with the code for the model and simulations](abm_comses).

Here's [a more extensive analysis](abm_htc/) of different rider densities and
different street networks.

Also check out some exciting animations:

-   <https://archive.org/details/ride_050bikes_cdneza>

-   <https://archive.org/details/ride_100bikes_xochimilco>

-   <https://archive.org/details/ride_100bikes_uacm>

-   <https://archive.org/details/ride_200bikes_cdneza>
