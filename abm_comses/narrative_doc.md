# Narrative documentation of the Social Cycling model

This agent based simulation has been developed to aid in the design of
"[Social Cycling](https://gitlab.com/rgarcia-herrera/social-cycling/)",
a mobile app that helps with the coordination of bike
riders so that flocks are assembled in a simple and spontaneous way.


Agent Based Modeling
====================

Agent Based Modeling is used to analyze the system’s behavior. The model
description follows the [ODD (Overview, Design concepts, Details) protocol](https://doi.org/10.1016/j.ecolmodel.2010.08.019).

Purpose
-------

The purpose of these simulations is to test if flocks of cyclists
are formed given the alterations to Craig Raynold’s original set of
rules for boids.

Original rules being:

1.  Avoid collisions with other boids.

2.  Match speed and heading of the flock by adopting an average of
	surrounding boids.

3.  Keep close to perceived center of flock.

To which this model adds:

4. Ride from current location to given destination (i.e. don't just wander around)

5. Consider nearby agents only if their destinations are close to mine.


Entities, state variables, and scales
----------------------------------------------------------------

The only entities are cyclicsts.

Their state variables are: location coordinates (lat, lon), destination
coordinates (lat, lon), speed in meters per second, heading (an angle),
status which may be **solo, seeking,** or **flocking**.

Simulations are run on real-world street network maps, agent locations
are latitude-longitude coordinates. Scale depends on desired zoom level.


### Density

Density refers to the amount of concurrent trips within an area during a
given period. It is normalized to trips per square kilometer per hour.

### Local and remote altruisms

Local altruism surrounds the user’s current location. Remote altruism
surrounds the user’s destination. These radii are a ratio of the
user’s trip total length and are set respectively to 0.1 and 0.2. A
user with a trip of 10km with a local altruism of 0.1 is willing to
ride an extra kilometer to join a flock. A remote altruism of 0.2
means she’s willing to flock with other riders even if she will stray
up to two kilometers from her destination. Since these values are
ratios of the trip length, finding flocks is likely when trips are
long and rare on the last hundred meters.

Process overview and scheduling
-------------------------------

The following program listing gives an overview of the simulation
process:

	for t in planned_steps:  # 1800 steps means half an hour
		for bike in N:  # N has all bike objects in the simulation
			if t % 10:
				bike.flocking_step()  # updates status
			bike.step()

 - Every thenth simulation step agents seek to flock with other agents.

   - The `flocking_step() ` method queries the bike database for nearby agents
	 with similar destinations.

	 - If found agents are within two times the agent's speed, and
	   their headings are similar to within four degrees, then status
	   is set to **flocking**, route is updated.

	 - If found agents are farther: status is set to **flocking**,
	   the centroid of their coordinates is computed and a route is
	   planned that goes through that centroid and then on to the
	   agent’s destination.

	 - If no nearby agents found, set status to **solo**, keep
	   current route.

 - Agents are initialized with a location point, a destination point,
   and a route that connects them. Calling the ` step() ` method will
   move an agent to the next point in its planned `route`, which is a
   list of geographic coordinates spaced to match the agents current
   speed.

Design concepts
---------------

### Basic principles

This models follows the same basic principles as [Craig Raynolds’ Boids flocking model](https://dl.acm.org/citation.cfm?id=37406): local
interactions give rise to flocking behaviour.

### Emergence

Flock formation is an emergent property of the model.

### Adaptation

Agents prefer the **flocking** state, and will periodically seek nearby
agents with similar destinations to flock with them.

### Objectives

Agents have two objectives: 1. Traveling from their current location to
their destination 2. Flocking with other agents.

### Learning

No learning mechanisms are modeled.

### Prediction

Agents will flock with other agents if they route through the centroid
of perceived flock candidates.

### Sensing

Agents perceive nearby agents and can select those with destinations
close to their own to flock with them.

### Interaction

Agents interact with each other by converging to the centroid of
perceived flock, and matching their speeds. Since they follow a
semi-common route their headings will also match while they belong to
a flock. Interaction with the street network is done by an external
routing program.

### Stochasticity

If agents are joining a flock (**flocking** status), speeds will
randomly decrease to a range of 1.5 to 2 m/s in order to wait for it if
they are ahead of the flock, or increase to 4.5 or 6 m/s if they are
behind, to catch up. Agents that have joined a flock (**flocking**
status) fix their speed at 3.5 m/s (12.6 km/h). Solo riders (**solo**
status) set their speed at 4 m/s.

### Collectives

Flocks are formed by agents that have the same heading and are within
a short distance of of each other.

### Observation

Agent variables under observation are: location (lat, lon),
status.and perceived flock sizes.

Initialization
--------------

 - Trips are setup with random lengths ranging 4 to 5 kilometers.
   Agents are thus initialized with an initial location, a
   destination, and a route that connects them.

 - Simulations are configured to run for 600 steps, i.e. ten minutes
   if each step represents one second.

 - Initial speed for agents is 4 m/s.

 - 125 agents are initialized for a density of 50 cyclists per sqare km per hour.

Input data
-------------------------------

The simulation take place in a 5 $km^{2}$ square, somewhere around
Mexico City's Autonomous University Del Valle Campus.

Input data for the simulation are street networks obtained using OSMNX.

Submodels
---------

### Routing model

The simulation framework uses geographical coordinates, which allows
it to run realistic simulations.

Since the focus of this study is the emergent formation of flocks,
route computation has been externalized by implementing a ready made
routing engine.

Agents navigate street networks by querying the router server
[Brouter](http://brouter.de).

BRouter specializes in bicycle trips. The default routing profile
considers terrain elevation changes, prefers bike pathds, routes
through parks, etc..
