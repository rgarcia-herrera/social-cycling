ABM simulation of the Social Cycling app
========================================

This agent based simulation has been developed to aid the design of
"[Social Cycling](https://gitlab.com/rgarcia-herrera/social-cycling/)",
a mobile app that helps with the coordination of bike
riders so that flocks are assembled in a simple and spontaneous way.

A description that follows the [ODD (Overview, Design concepts, Details) protocol](https://doi.org/10.1016/j.ecolmodel.2010.08.019) can be read in the [narrative document](narrative_doc.md).

You may examine the code for the model and a simulation run as a
[static render of a Jupyter Notebook online](https://gitlab.com/rgarcia-herrera/social-cycling/blob/master/abm_comses/social_cycling.ipynb). Or
you can run it localy following instructions in the next section.


Installation and running
========================

Unpackage and *cd* into the extracted social_cycling directory. It should contain these files:

	$ ls
    README.md         brouter-1.4.11.tar.bz2   narrative_doc.pdf  requirements.txt  social_cycling.ipynb
    narrative_doc.md  render_frames.py
	$

Start BRouter server
--------------------

Unpack it first:

	$ tar xvfj brouter-1.4.11.tar.bz2
	[...]

Start it with included script:

	$ cd brouter-1.4.11/standalone/
	$ ./server.sh
	BRouter 1.4.11 / 02042018

This command will keep the BRouter server running in the
foreground. To stop it type ctrl-c or close the terminal window.

System Dependencies
------------

This program uses [OSMNX](https://pypi.org/project/OSMnx/) which in
turn depends on the [libspatialindex](https://libspatialindex.org/)
library.

Also using a Python [Virtualenv](http://virtualenv.org) is highly
recommended.

To install both on a [Debian](https://www.debian.org) system, use this
command:

	sudo apt-get install libspatialindex-dev python-virtualenv

Other Debian-based distros (such as Ubuntu) probably have the exact
same packages available.

Python Dependencies
-------------------

First create a Python virtual environment, where libraries will be installed:

	$ virtualenv -p python2.7 venv
	[...]
	Installing setuptools, pkg_resources, pip, wheel...done.
	$

Activate the environment:

	$ source venv/bin/activate
	(venv) $

Install libraries:

	(venv) $ pip install -r requirements.txt
	[...]

Start Juypter Notebook
----------------------

This command will start a jupyter server and keep it running in the
foreground. It will also open a browser and point it to the actual
notebook.

	(venv) $ jupyter notebook social_cycling.ipynb
	[...]
