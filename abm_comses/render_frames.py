import argparse
import pandas as pd
import matplotlib.pyplot as plt
import os

import osmnx as ox
ox.config(log_file=True, log_console=True, use_cache=True)


parser = argparse.ArgumentParser(
    description='Render frames from a simulation run.')

parser.add_argument('--csv', type=argparse.FileType('r'), nargs="+",
                    help='paths to observation data files')

parser.add_argument('--lat', default=19.3838,
                    help='latitude at center of square')

parser.add_argument('--lon', default=-99.1758,
                    help='longitude at center of square')

parser.add_argument('--distance', default=2500,
                    help='distance from center to each edge of square in meters')

parser.add_argument('--outdir', required=True,
                    help='directory path to output frames')

args = parser.parse_args()


# Setting a distance of 2500 creates a square bounding box with 5km on each side.
G = ox.graph_from_point((args.lat, args.lon), distance=2500)

# create output dir
if not os.path.isdir(args.outdir):
    os.makedirs(args.outdir)

# prepare map for background
gdf = ox.plot.graph_to_gdfs(G, nodes=False)

# load agent trails
observe = {}
for b in args.csv:
    observe[id(b)] = pd.read_csv(b)

color_fl = '#e41a1c'
color_so = '#377eb8'
color_se = '#4daf4a'
    
# render a frame for each time step
for t in range(len(observe[id(b)])):
    fig = plt.figure(dpi=150)

    ax = plt.gca()
    
    # plot streets
    gdf.plot(ax=ax, linewidth=0.1, color="grey")
   
    # mark agent locations at current timestep
    for k in observe:

        if observe[k].loc[t]['status'] == 'solo':
            color = color_so
            size = 0.0002
        elif observe[k].loc[t]['status'] == 'seeking':
            color = color_se
            size = 0.0002
        elif observe[k].loc[t]['status'] == 'flocking':
            color = color_fl
            size = 0.0002 * observe[k].loc[t]['flock_size'] * 2
        
        if observe[k].loc[t]['status'] is not 'arrived':
            ax.add_artist(plt.Circle((observe[k].loc[t]['lon'],
                                      observe[k].loc[t]['lat']),
                                     size, color=color, alpha=0.65))

    ax.set_xticks([]) 
    ax.set_yticks([]) 

    figname = os.path.join(args.outdir, 'frame_%05i.png' % t)
    fig.savefig(figname)

    print "wrote %s" % figname
    
    plt.close(fig)

    
# these frames can be rendered into a video using ffmpeg (ffmpeg.org) like so:
# $ cd output/dir
# $ ffmpeg -r 15 -start_number 00000 -i frame_%05d.png ride.mp4
# will render at 15 frames per second
